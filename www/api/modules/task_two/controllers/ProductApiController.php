<?php

namespace app\modules\task_two\controllers;

use yii\rest\ActiveController;

class ProductApiController extends ActiveController {

    public $modelClass = 'app\modules\task_two\models\Product';

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['contentNegotiator'] = [
            'class' => 'yii\filters\ContentNegotiator',
            'formats' => [
                'application/json' => \yii\web\Response::FORMAT_JSON,
            ]
        ];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();


        $actions['index']['dataFilter'] = [
            'class' => \yii\data\ActiveDataFilter::class,
            'searchModel' => $this->modelClass,
        ];

        return $actions;
    }
    
}