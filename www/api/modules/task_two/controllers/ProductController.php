<?php

namespace app\modules\task_two\controllers;

use yii\web\Controller;
use app\modules\task_two\models\Product;

class ProductController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['contentNegotiator'] = [
            'class' => 'yii\filters\ContentNegotiator',
            'formats' => [
                'application/json' => \yii\web\Response::FORMAT_JSON,
            ]
        ];
        return $behaviors;
    }

    public function actionGetActiveProducts($categoryId) {

        $query = Product::find()
            ->andWhere([Product::tableName().'.active'=> 1])
            ->andWhere([Product::tableName().'.category_id'=> $categoryId])
            ->asArray();

        $data = \Yii::$app->cache->getOrSet('categories'.$categoryId, function () use ($query) {
            return $query->all();
        },600);
        return $data;

    }

}