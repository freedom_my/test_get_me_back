<?php
namespace app\modules\task_two;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\task_two\controllers';
}