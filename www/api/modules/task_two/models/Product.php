<?php
namespace app\modules\task_two\models;

use yii\db\ActiveRecord;

class Product extends ActiveRecord
{
    public static function tableName()
    {
        return '{{product}}';
    }

    public function rules() {
        return [
            ['active', 'integer'],
            ['name', 'string'],
            ['category_id', 'integer'],
        ];
    }
}