# Реализация задачи 2

Задача реализованна в двух вариантах : 
Первый (как бы я сделал максимально быстро и без кеша на activeController) :
[Задача 2](http://185.211.170.169:8080/task_two/product-api/index?filter[category_id]=нужная_категория&filter[active]=активность)

Второй (с реализацией кеширования на Controller) :
[Задача 2](http://185.211.170.169:8080/task_two/product/get-active-products?categoryId=нужная_категория)