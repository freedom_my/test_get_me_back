<?php

namespace app\modules\task_one\services;

use app\modules\task_one\models\active_records\CheckRecord;
use app\modules\task_one\models\active_records\CheckElemRecord;

use app\modules\task_one\services\dto\CheckCreateDto;
use app\modules\task_one\services\dto\CheckDiscountDto;

use app\modules\task_one\helpers\CheckHelper;

class CheckService {

    private $check;
    private $checkElemService;

    public function __construct(CheckRecord $check, CheckElemService $checkElemService) {
        $this->check = $check;
        $this->checkElemService = $checkElemService;
    }

    public function create(CheckCreateDto $dto) {
        $dto->type = CheckHelper::getTypeCheck($dto->check);
        $this->check->add($dto);
        $this->check->save();

        foreach (CheckHelper::parseCheck($dto->check, $dto->type) as $checkElemDto) {
            $checkElemDto->check_id = $this->check->id;
            $this->checkElemService->create($checkElemDto);
            $this->check->price += $checkElemDto->price;
        }

        $this->check->save();

        return $this->check->id;
    }

    public function getDiscount(CheckDiscountDto $dto) {

        $elems = CheckElemRecord::find()
            ->andWhere([CheckElemRecord::tableName().'.check_id' => $dto->id])
            ->all();

        $discountByElem = CheckHelper::getDiscountByN((int)$dto->n, count($elems));

        foreach ($elems as &$elem) {
            $elem->price -= CheckHelper::getDiscountByM((int)$dto->m, $elem->price);
            $elem->price -= $discountByElem;
        }

        $dto->check = CheckHelper::createCheck($elems, $dto->type);
        return $dto;
    }
}