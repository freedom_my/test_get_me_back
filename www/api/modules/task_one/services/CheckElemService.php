<?php

namespace app\modules\task_one\services;

use app\modules\task_one\models\active_records\CheckRecord;
use app\modules\task_one\services\dto\CheckElemCreateDto;
use app\modules\task_one\models\active_records\CheckElemRecord;

class CheckElemService {

    private $checkElem;

    public function __construct(CheckElemRecord $checkElem) {
        $this->checkElem = $checkElem;
    }

    public function create(CheckElemCreateDto $dto) {
        $this->checkElem = new CheckElemRecord();
        $this->checkElem->add($dto);
        $this->checkElem->save();
    }
}