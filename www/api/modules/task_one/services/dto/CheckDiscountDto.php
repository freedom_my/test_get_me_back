<?php

namespace app\modules\task_one\services\dto;

class CheckDiscountDto {
    public $check;
    public $type;
    public $id;
    public $m;
    public $n;
    public $price;
}