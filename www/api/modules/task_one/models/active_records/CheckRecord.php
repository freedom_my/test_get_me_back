<?php

namespace app\modules\task_one\models\active_records;

use yii\db\ActiveRecord;
use yii;
use app\modules\task_one\services\dto\CheckDiscountDto;

class CheckRecord extends ActiveRecord{

    const TYPE_ONE = 1;
    const TYPE_TWO = 2;

    public function attributeLabels() {
        return [
            'check' => Yii::t('app','Текст чека'),
        ];
    }

    public static function tableName()
    {
        return '{{check}}';
    }
    

    public function add($dto) {
        $this->check = $dto->check;
        $this->type = $dto->type;
        $this->price = $dto->price;
    }

    public function getDto()
    {
        $dto = new CheckDiscountDto();
        $dto->check = $this->check;
        $dto->type = $this->type;
        $dto->id = $this->id;
        $dto->price = $this->price;
        return $dto;
    }
}