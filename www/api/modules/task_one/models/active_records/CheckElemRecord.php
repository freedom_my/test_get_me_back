<?php

namespace app\modules\task_one\models\active_records;

use yii\db\ActiveRecord;

class CheckElemRecord extends ActiveRecord{

    public static function tableName()
    {
        return '{{check_elem}}';
    }

    public function rules() {
        return [
            ['check_id', 'integer'],
            ['name', 'string'],
            ['count', 'integer'],
            ['price', 'integer'],
        ];
    }
    public function add($dto) {
        $this->price = $dto->price;
        $this->name = $dto->name;
        $this->count = $dto->count;
        $this->check_id = $dto->check_id;
    }
}