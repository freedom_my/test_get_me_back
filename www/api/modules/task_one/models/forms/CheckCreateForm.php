<?php

namespace app\modules\task_one\models\forms;

use yii\base\Model;
use app\modules\task_one\services\dto\CheckCreateDto;

class CheckCreateForm extends Model {

    public $check;
    public $type;

    public function rules() {
        return
            [
                [['check'], 'safe'],
            ];
    }

    public function getDto()
    {
        $dto = new CheckCreateDto();
        $dto->check = $this->check;
        $dto->price = 0;
        return $dto;
    }

    
}