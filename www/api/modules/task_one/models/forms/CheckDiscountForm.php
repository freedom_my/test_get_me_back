<?php

namespace app\modules\task_one\models\forms;

use yii\base\Model;
use app\modules\task_one\services\dto\CheckDiscountDto;

class CheckDiscountForm extends Model {

    public $id;
    public $check;
    public $type;
    public $m;
    public $n;
    public $price;

    public function rules() {
        return
            [
                [['check'], 'safe'],
                [['type'], 'integer'],
                [['m'], 'integer'],
                [['n'], 'integer'],
                [['id'], 'integer'],
            ];
    }

    public function getDto()
    {
        $dto = new CheckDiscountDto();
        $dto->check = $this->check;
        $dto->type = $this->type;
        $dto->m = $this->m;
        $dto->n = $this->n;
        $dto->id = $this->id;
        $dto->price = $this->price;
        return $dto;
    }

    public function setDto($dto)
    {
        $this->id = $dto->id;
        $this->check = $dto->check;
        $this->type = $dto->type;
        $this->price = $dto->price;
    }
}