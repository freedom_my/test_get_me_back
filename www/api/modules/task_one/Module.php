<?php
namespace app\modules\task_one;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\task_one\controllers';
}