<?php

namespace app\modules\task_one\helpers;

use app\modules\task_one\services\dto\CheckElemCreateDto;

class CheckHelper {

    public static function parseCheck($check, $type) {

        switch ( $type) {
            case 1 :
                return self::parseCheckOne($check);
            case 2 :
                return self::parseCheckTwo($check);
            default :
                return [];
        }
    }

    public static function createCheck($elems, $type) {

        switch ( $type) {
            case 1 :
                return self::createCheckOne($elems);
            case 2 :
                return self::createCheckTwo($elems);
            default :
                return [];
        }
    }

    public static function getTypeCheck($check) {
        if (strripos($check,',')) {
            return 1;
        }
        return 2;
    }

    public static function parseCheckOne($check) {
        $result = [];

        $checkElems = array_map('trim', explode(',', $check));


        foreach ($checkElems as $checkElem) {
            $checkElemDataArr = explode(' ', $checkElem);

            $result[] = new CheckElemCreateDto();

            current($result)->count = 1;
            current($result)->price = (int)($checkElemDataArr[1]);
            current($result)->name = $checkElemDataArr[0];
            next($result);
        }
        return $result;
    }

    public static function parseCheckTwo($check) {
        $result = [];

        $checkElems = array_filter(array_map('trim', explode('₽', $check)), function($element) {
            return !empty($element);
        });


        foreach ($checkElems as $checkElem) {

            $checkElemDataArr = explode(' ', $checkElem);
            $result[] = new CheckElemCreateDto();
            current($result)->count = (int)($checkElemDataArr[1]);
            current($result)->price = (int)($checkElemDataArr[2]);
            current($result)->name = $checkElemDataArr[0];
            next($result);
        }
        
        return $result;
    }

    public static function getDiscountByN($n, $count) {
        return round($n / $count, 2);
    }

    public static function getDiscountByM($m, $price) {
        return round($m * $price / 100, 2);
    }

    public static function createCheckOne($elems) {
        $result = '';

        foreach ($elems as $key => $elem) {
            $result .= $elem->name.' ';
            $result .= $elem->price.'₽';
            if ($key == count($elems) - 1) {
                $result .= '.';
            } else {
                $result .= ', ';
            }
        }
        return $result;
    }

    public static function createCheckTwo($elems) {
        $result = '';

        foreach ($elems as $key => $elem) {
            $result .= $elem->name.' ';
            $result .= $elem->count.'шт. ';
            $result .= $elem->price.'₽ ';
        }
        return $result;
    }
}