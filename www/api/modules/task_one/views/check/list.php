<?php
use  yii\grid\GridView;
use \yii\helpers\Html;
use \yii\helpers\Url;

/* @var $dataProvider yii\web\View */

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'id',
        'check',
        'price',
        [
            'attribute' => Yii::t('app','Расчет скидки по чеку'),
            'format' => 'raw',
            'value' => function($data) {
                return Html::a(Yii::t('app','Расчет скидки по чеку'), Url::to(['/task_one/check/get-discount', 'id' => $data->id]));
            },
        ],
    ],
]);