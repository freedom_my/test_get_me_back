<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $model yii\web\View */


?>
<div>
    <?=$model->check?>
</div>
<?php
$form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'm') ?>
    <?= $form->field($model, 'n') ?>
    <?= Html::submitButton(Yii::t('app',Yii::t('app','Посчитать скидку'))) ?>
<?php ActiveForm::end(); ?>