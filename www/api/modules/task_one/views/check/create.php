<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $model yii\web\View */

$form = ActiveForm::begin(); ?>
<?= $form->field($model, 'check') ?>
<?= Html::submitButton(Yii::t('app','Заполнить чек')) ?>
<?php ActiveForm::end(); ?>