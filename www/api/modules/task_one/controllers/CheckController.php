<?php

namespace app\modules\task_one\controllers;

use yii;
use yii\web\Controller;

use app\modules\task_one\services\CheckService;
use app\modules\task_one\models\forms\CheckCreateForm;
use app\modules\task_one\models\forms\CheckDiscountForm;

use app\modules\task_one\models\active_records\CheckRecord;

use yii\data\ActiveDataProvider;

use yii\web\NotFoundHttpException;

class CheckController extends Controller
{
    public $layout = 'test_one';

    private $checkService;

    public function __construct($id, $module, CheckService $checkService, $config = []) {
        $this->checkService = $checkService;
        parent::__construct($id, $module, $config);
    }

    public function actionCreate() {
        $form = new CheckCreateForm();

        if ($form->load(\Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->checkService->create($form->getDto());
                return $this->redirect(['list']);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
            }
        }

        return $this->render('create', [
            'model' => $form,
        ]);
    }

    public function actionList() {
        $dataProvider = new ActiveDataProvider([
            'query' => CheckRecord::find(),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        return $this->render('list', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionGetDiscount($id) {

        $check = CheckRecord::findOne($id);

        if ($check) {

            $form = new CheckDiscountForm();
            $form->setDto($check->getDto());

            if ($form->load(\Yii::$app->request->post()) && $form->validate()) {
                try {
                    $dtoDiscount = $this->checkService->getDiscount($form->getDto());
                    $form->check = $dtoDiscount->check;

                    return $this->render('get-discount', [
                        'model' => $form,
                    ]);
                } catch (\DomainException $e) {
                    Yii::$app->errorHandler->logException($e);
                }
            }

            return $this->render('get-discount', [
                'model' => $form,
            ]);
        }

        throw new NotFoundHttpException(Yii::t('app', 'Страница не найдена.'),404);
    }
}