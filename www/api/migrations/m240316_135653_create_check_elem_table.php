<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%check_elem}}`.
 */
class m240316_135653_create_check_elem_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%check_elem}}', [
            'id' => $this->primaryKey(),
            'check_id' => $this->integer(),
            'count' => $this->integer(),
            'price' => $this->integer(),
            'name' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%check_elem}}');
    }
}
