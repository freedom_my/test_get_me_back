<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%check}}`.
 */
class m240316_135646_create_check_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%check}}', [
            'id' => $this->primaryKey(),
            'check' => $this->text(),
            'type' => $this->integer(),
            'price' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%check}}');
    }
}
