<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%product}}`.
 */
class m240316_191414_create_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%product}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(),
            'name' => $this->string(),
            'active' => $this->tinyinteger(),
        ]);
        $this->batchInsert('product', ['category_id', 'name', 'active'], [
            ['1', 'Ботинки', '1'],
            ['4', 'Кресло', '1'],
            ['2', 'Автомобиль', '0'],
            ['2', 'Игрушка', '1'],
            ['1', 'Коляска', '1'],
            ['4', 'Диван', '1'],
            ['3', 'Дверь', '0'],
            ['4', 'Стол', '1'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%product}}');
    }
}
